import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { DetailTable } from "../components/DetailTable";


export default function ListManufactures() {
    const [details, setDetails] = useState([])
    const includedValues = ["name", "#_on_lot", "#_sold"]
    const buttons = [{"name": "Delete", "class": "btn btn-danger", "click": handleDelete}]

    async function handleDelete(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8100/api/manufacturers/${event.target.value}/`, fetchConfig)
        if (response.ok) {
            const copyData = [...details]
            setDetails(copyData.filter((detail) => detail.id != id))
        }

    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/")
        if (response.ok) {
            const data = await response.json()
            setDetails(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{margin: "1rem"}}>
                <h3>Manufacturers:</h3>
                <Link to="new" className="btn btn-success btn-lg px-4 gap-3">Add a Manufacturer</Link>
            </div>
            <hr/>
            {details.length > 0 ? DetailTable(details, includedValues, buttons, "id") : null}
        </div>
    )
}