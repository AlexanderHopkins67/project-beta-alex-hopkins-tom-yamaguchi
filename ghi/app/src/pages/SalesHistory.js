import React, { useEffect, useState } from 'react';

export default function SalesPersonHistory() {
  const [sales, setSales] = useState([]);
  const [salesData, setSaleData] = useState([]);
  const [salespeople, setSalespeople] = useState([]);

  const fetchSalesData = async () => {
    try {
      const response = await fetch("http://localhost:8090/api/sales/");
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
        setSaleData(data.sales);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const fetchSalespeopleData = async () => {
    try {
      const response = await fetch("http://localhost:8090/api/salespeople/");
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchSalesData();
    fetchSalespeopleData();
  }, []);

  const handleSalespersonSelect = (event) => {
    const value = event.target.value;
    let updatedSales;
    if (value === "all") {
      updatedSales = salesData;
    } else {
      updatedSales = salesData.filter(
        (obj) => obj.salesperson.id === parseFloat(value)
      );
    }
    setSales(updatedSales);
  };

  return (
    <div className="container">
      <div className="salesperson-history">
        <h1>Employee Sales Records</h1>
        <div className="filter-container">
          <label htmlFor="salesperson" className="salesperson-label">
            Select Salesperson:
          </label>
          <select
            onChange={handleSalespersonSelect}
            required
            id="salesperson"
            name="salesperson"
            className="salesperson-select"
          >
            <option value="all" key="all">
              All
            </option>
            {salespeople?.map((salesperson) => (
              <option value={salesperson.id} key={salesperson.id}>
                {`${salesperson.first_name} ${salesperson.last_name}`}
              </option>
            ))}
          </select>
        </div>
        <table className="sales-table">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {sales?.map((sale) => (
              <tr key={sale.id}>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>${sale.price}.00</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
