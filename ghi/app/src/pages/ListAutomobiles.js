import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import { DetailTable } from "../components/DetailTable";

export default function ListAutomobiles() {
    const [details, setDetails] = useState([])
    const includedValues = ["vin", "color", "model", "manufacturer", "sold"]
    const buttons = [{"name": "Delete", "class": "btn btn-danger", "click": handleDelete}]

    async function handleDelete(event) {
        const id = event.target.value
        const fetchConfig = {
            method: "delete",
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch(`http://localhost:8100/api/automobiles/${event.target.value}/`, fetchConfig)
        if (response.ok) {
            const copyData = [...details]
            setDetails(copyData.filter((detail) => detail.vin != id))
        }

    }

    const fetchData = async () => {
        const response = await fetch("http://localhost:8100/api/automobiles/")
        if (response.ok) {
            const data = await response.json()
            let formattedData = data.autos.map((item) => {
                item.manufacturer = item.model.manufacturer.name
                item.model = item.model.name
                item.sold = item.sold ? "Yes" : "No"
                return item

            })
            setDetails(formattedData)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])


    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{margin: "1rem"}}>
                <h3>Automobiles:</h3>
                <Link to="new" className="btn btn-success btn-lg px-4 gap-3">Add an Automobile</Link>
            </div>
            <hr/>
            {details ? DetailTable(details, includedValues, buttons, "vin") : null}
        </div>
    )
}