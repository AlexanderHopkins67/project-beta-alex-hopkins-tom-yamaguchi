import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

export default function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([]);

    const fetchSalesPeopleData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        } else {
            console.error(response);
        }
    };

    useEffect(() => {
        fetchSalesPeopleData();
    }, []);

    const deletePerson = async (event, id) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/salespeople/${id}`;
        const response = await fetch(url, { method: "DELETE" });
        if (response.ok) {
            setSalespeople(salespeople.filter((person) => person.id !== id));
        }
    };

    return (
        <div className="container">
            <div className="d-grid gap-2 d-sm-flex justify-content-between" style={{ margin: "1rem" }}>
                <Link to="new" className="btn btn-primary">
                    Add a new salesperson
                </Link>
            </div>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople?.map((person, index) => (
                        <tr
                            key={person.id}
                            style={index % 2 === 0 ? { backgroundColor: 'lightgrey' } : null}
                        >
                            <td>{person.employee_id}</td>
                            <td>{person.first_name}</td>
                            <td>{person.last_name}</td>
                            <td>
                                <button className="btn btn-danger" onClick={(event) => deletePerson(event, person.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}
