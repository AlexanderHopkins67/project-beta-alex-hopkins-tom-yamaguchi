import React, { useState } from 'react';

export default function SalespeopleForm() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [employeeId, setEmployeeId] = useState("");

  const handleSubmit = async event => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId
    };

    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    const response = await fetch(salespeopleUrl, fetchConfig);
    if (response.ok) {
      setFirstName("");
      setLastName("");
      setEmployeeId("");
    } else {
      console.error(response);
    }
  };

  const handleFirstNameChange = event => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = event => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeIdChange = event => {
    const value = event.target.value;
    setEmployeeId(value);
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-8">
          <div className="shadow p-4 mt-4">
            <h1 className="text-center mb-4">Add a Salesperson</h1>
            <form onSubmit={handleSubmit} id="create-new-salesperson">
              <div className="form-floating mb-3">
                <input
                  value={firstName}
                  onChange={handleFirstNameChange}
                  placeholder="First Name"
                  required
                  type="text"
                  id="first_name"
                  name="first_name"
                  className="form-control"
                />
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={lastName}
                  onChange={handleLastNameChange}
                  placeholder="Last Name"
                  required
                  type="text"
                  id="last_name"
                  name="last_name"
                  className="form-control"
                />
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={employeeId}
                  onChange={handleEmployeeIdChange}
                  placeholder="Employee ID"
                  required
                  type="text"
                  id="employeeId"
                  name="employeeId"
                  className="form-control"
                />
                <label htmlFor="employeeId">Employee ID</label>
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-outline-primary">
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
