import React, { useState, useEffect } from "react";


export default function ModelForm() {
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    async function fetchManu() {
      const url = 'http://localhost:8100/api/manufacturers/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      }
    }
    fetchManu();
  }, [])

  async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            "name": name,
            "picture_url": pictureUrl,
            "manufacturer_id": manufacturer
        };
        console.log(data);

        const modelsUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        console.log(fetchConfig);

        const response = await fetch(modelsUrl, fetchConfig);

        console.log(response);

        if (response.ok) {

            const newModel = await response.json();
            console.log(newModel);
            setName('');
            setPictureUrl('');
            setManufacturer('');

        }
  }
  function handleName(event){
    const value = event.target.value;
    setName(value);
  };

  function handlePictureUrl(event){
    const value = event.target.value;
    setPictureUrl(value);
  };

  function handleManufacturer(event) {
    const value = event.target.value;
    setManufacturer(value);
  };


  return (
    <div className="shadow">
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleName}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePictureUrl}
                value={pictureUrl}
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
            <select
                onChange={handleManufacturer}
                value={manufacturer}
                required name="manufacturer"
                id="manufacturer"
                className="form-control"
              >
                <option value="">Select a manufacturer</option>
                {manufacturers.map((manufacturer) => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                            {manufacturer.name}
                        </option>
                    )
                })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    </div>
  );
}
