import { useDefaultState } from "../../components/hooks";
import { useRef, useEffect } from "react";
import { Link } from 'react-router-dom';
import "./forms.css"

export default function TechnicianForm() {
    const first_name = useDefaultState()
    const last_name = useDefaultState()
    let success = useRef(false)

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {
            "first_name": first_name.state,
            "last_name": last_name.state
        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'application/json',
            }
        }
        const response = await fetch("http://localhost:8080/api/technicians/", fetchConfig)
        if (response.ok) {
            first_name.reset()
            last_name.reset()
            success.current = true
        }
    }

    useEffect(() => {
        success.current = false
    }, [first_name])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow mt-4 d-flex flex-column bg-success rounded-3">
                    <h1 className="align-self-center text-white p-1">Hire a new Technician</h1>
                    <form className="p-4 bg-light d-flex flex-column" onSubmit={handleSubmit}>
                        { success.current ?
                            <div className="alert alert-success" role="alert">
                                <strong>Success!</strong> New Technician Hired!
                            </div>
                            :
                            null
                        }
                        <div className="form-floating mb-3">
                            <input
                                value={first_name.state}
                                onChange={first_name.handleChange}
                                placeholder="First Name"
                                required type="text"
                                name="first_name"
                                id="first_name"
                                className="form-control"
                                />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={last_name.state} onChange={last_name.handleChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="d-flex flex-row justify-content-around">
                            <button className="btn btn-success w-25">Hire</button>
                            <Link to="../technicians" className="btn btn-secondary w-25">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}