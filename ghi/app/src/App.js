import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './components/MainPage';
import Nav from './components/Nav';
import ListAutomobiles from './pages/ListAutomobiles';
import AutomobileForm from './pages/forms/AutomobileForm';
import ListManufactures from './pages/ListManufacturers';
import ManufacturerForm from './pages/forms/ManufacturerForm';
import ListModels from './pages/ListModels';
import ModelForm from './pages/forms/ModelForm';


import ListTechnicians from './pages/ListTechnicians';
import TechnicianForm from './pages/forms/TechnicianForm';
import ListServiceAppointments from './pages/ServiceAppointments';
import ServiceAppointmentForm from './pages/forms/ServiceAppointmentForm';
import ServiceHistory from './pages/ServiceHistory';

import ListSales from './pages/ListSales';
import SaleForm from './pages/forms/SaleForm';
import ListSalespeople from './pages/ListSalespeople';
import SalespersonForm from './pages/forms/SalespersonForm';
import ListCustomers from './pages/ListCustomers';
import CustomerForm from './pages/forms/CustomerForm';
import SalesHistory from './pages/SalesHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="inventory">
            <Route path="automobiles" element={<ListAutomobiles />} />
            <Route path="automobiles/new" element={<AutomobileForm />} />
            <Route path="manufacturers" element={<ListManufactures />} />
            <Route path="manufacturers/new" element={<ManufacturerForm />} />
            <Route path='models' element={<ListModels />} />
            <Route path="models/new" element={<ModelForm />} />
          </Route>

          <Route path="service-department">
            <Route path="technicians" element={ <ListTechnicians /> } />
            <Route path="technicians/new" element={ <TechnicianForm /> } />
            <Route path="appointments" element={ <ListServiceAppointments /> } />
            <Route path="appointments/new" element={ <ServiceAppointmentForm /> } />
            <Route path="history" element={ <ServiceHistory /> } />
          </Route>

          <Route path="sales-department">
            <Route path="sales" element={ <ListSales /> } />
            <Route path="sales/new" element={ <SaleForm /> } />
            <Route path="salespeople" element={ <ListSalespeople /> } />
            <Route path="salespeople/new" element={ <SalespersonForm /> } />
            <Route path="customers" element={ <ListCustomers /> } />
            <Route path="customers/new" element={ <CustomerForm /> } />
            <Route path="history" element={ <SalesHistory /> } />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
