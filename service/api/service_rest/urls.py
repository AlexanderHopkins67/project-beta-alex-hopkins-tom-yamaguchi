from django.urls import path
from .views import create_list_techs, create_list_appointment, delete_tech, delete_appointment, set_canceled, set_finished

urlpatterns = [
    path("technicians/", create_list_techs, name="List_Create_Technicians"),
    path("technicians/<int:id>/", delete_tech, name="Delete Technician"),
    path("appointments/", create_list_appointment, name="List_Create_Appointments"),
    path("appointments/<int:id>/", delete_appointment, name="Delete_Appointments"),
    path("appointments/<int:id>/cancel/", set_canceled, name="Canceled"),
    path("appointments/<int:id>/finish/", set_finished, name="Finished"),

]