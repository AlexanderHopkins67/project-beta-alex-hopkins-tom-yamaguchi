from django.contrib import admin
from .models import AutomobileVO, Appointment, Technician

# Register your models here.
admin.site.register(AutomobileVO)
admin.site.register(Technician)
admin.site.register(Appointment)