# Generated by Django 4.0.3 on 2023-06-07 23:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_remove_technician_id_alter_technician_employee_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='customer',
        ),
        migrations.AddField(
            model_name='appointment',
            name='customer_href',
            field=models.CharField(default='null', max_length=100),
            preserve_default=False,
        ),
    ]
