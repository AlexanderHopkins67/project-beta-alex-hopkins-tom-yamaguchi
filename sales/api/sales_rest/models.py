
# Create your models here.
from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, blank=True, null=True,unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return f"/api/automobiles/{self.pk}/"



class Salesperson(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    employee_id = models.PositiveBigIntegerField(unique=True)

    def __str__(self):
        return self.first_name + self.last_name

    def get_api_url(self):
        return f"/api/salespeople/{self.pk}/"


class Customer(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)

    def get_api_url(self):
        return f"/api/customers/{self.id}/"

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Sale(models.Model):
    automobile = models.ForeignKey(
        'AutomobileVO',
        related_name='sales',
        on_delete=models.CASCADE)
    salesperson = models.ForeignKey(
        Salesperson,
        related_name='sales',
        on_delete=models.CASCADE
        )
    customer = models.ForeignKey(
        Customer,
        related_name='sales',
        on_delete=models.CASCADE,
        )
    price = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.salesperson} sold {self.automobile} to {self.customer} for ${self.price}"

    def get_api_url(self):
        return f"/api/sales/{self.pk}/"
