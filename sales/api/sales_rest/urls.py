from django.urls import path
from .views import (
    salesperson_list,
    delete_salesperson,
    api_customer_list,
    api_customer_detail,
    sales_history,
    delete_sale,


)

urlpatterns = [
    path("salespeople/", salesperson_list, name="salesperson_list"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:id>/", api_customer_detail, name="api_customer_detail"),
    path("sales/", sales_history, name="sales_records"),
    path("sales/<int:pk>/", delete_sale, name="delete_sale"),


]
