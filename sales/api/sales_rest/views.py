from itertools import count
from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse, Http404
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods

class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['import_href', 'vin', 'sold']
    encoders = {}

    def get_extra_data(self, o):
        return {
            "href": o.get_api_url(),
        }

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ['id', 'first_name', 'last_name', 'employee_id']
    encoders = {}

    def get_extra_data(self, o):
        return {
            "href": o.get_api_url(),
        }

@require_http_methods(["GET", "POST"])
def salesperson_list(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False
        )

    else:
        try:
            response = json.loads(request.body)
            salespeople = Salesperson.objects.create(**response)
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Exception as err:
            return JsonResponse(
                {"message": str(err)},
                status=404,
            )

@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    count, _ = get_object_or_404(Salesperson, id=id).delete()
    return JsonResponse(
        {"deleted": count > 0},
        status=200,
    )

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['id', 'first_name', 'last_name', 'address', 'phone_number']
    encoders = {}

    def get_extra_data(self, o):
        return {
            "href": o.get_api_url(),
        }



@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer_detail(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404
            )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            Customer.objects.filter(id=id).update(**content)
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer Input"},
                status=400
            )
    else:
        count, _ = Customer.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ['id', 'automobile', 'salesperson', 'customer', 'price']
    encoders = {
        "automobile": AutomobileEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),

    }

    def get_extra_data(self, o):
        return {
            "href": o.get_api_url(),
        }

@require_http_methods(["GET", "POST"])
def sales_history(request, employee_id=None):
    if request.method == "GET":
        if employee_id:
            salesperson = get_object_or_404(Salesperson, employee_id=employee_id)
            sales = Sale.objects.filter(salesperson=salesperson)
        else:
            sales = Sale.objects.all()

        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )

    else:
        try:
            print("try1")
            content = json.loads(request.body)
            automobile = get_object_or_404(AutomobileVO, vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=404,
            )
        try:
            salesperson = get_object_or_404(Salesperson, employee_id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=404,
            )

        try:
            print("try3")
            customer = get_object_or_404(Customer, id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )

        sale = Sale.objects.create(**content)
        if sale:
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
                safe=False,
            )

        else:
            return JsonResponse(
                {"message": "Could not create the sale"},
                status=404,
            )


@require_http_methods(["GET", "DELETE"])
def delete_sale(request, pk):
    count, _ = get_object_or_404(Sale, pk=pk).delete()
    return JsonResponse(
        {"deleted": count > 0},
        status=200,
    )

@require_http_methods(["GET"])
def salesperson_history(request, salesperson_id):
    try:
        salesperson = get_object_or_404(Salesperson, pk=salesperson_id)
        sales = Sale.objects.filter(salesperson=salesperson)
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Salesperson does not exist"},
            status=404,
        )
    except Exception as err:
        return JsonResponse(
            {"message": str(err)},
            status=400,
        )
