# CarCar
Built for car dealerships as a way to maintain thier inventory, services, sales, and customers.
It consits of 3 microservices built into a single interactive page.
Inventory consits each manufacturer, model, and vehicle, and updates if a car has been sold
Sales tracks customers, salespeople and their sales history, and customer info
Services contains technicians to hire(or fire), creating appointments page.
* Alex Hopkins - Service
* Tom Yamaguchi - Sales

## Requirements

​
**Required to Run:**

```
Docker
Git
Node.18.16.0
```

**Recommended Applications:**

```
Insomnia
```

## Starting the Application

1. Fork this repository
​
2. Clone the forked repository onto your computer:
git clone <<respository.url.here>>
​
3. Initialize Docker with these commands:

```
docker volume create beta-data
docker-compose build
docker-compose up
```

**Verify all Docker containers are running before proceeding, container on Port:3000 may take several minutes to fully start.**

## URLs & Endpoints

--------

## Browser URLs

| Path    | Page |
| :---    | ---- |
| <http://localhost:3000/> | Home |
| <http://localhost:3000/inventory/manufacturers> | Manufacturer List |
| <http://localhost:3000/inventory/manufacturers/new> | Manufacturer Creation Form |
| <http://localhost:3000/inventory/models> | Model List |
| <http://localhost:3000/inventory/models/new> | Model Creation Form |
| <http://localhost:3000/inventory/automobiles> | Automobile List |
| <http://localhost:3000/inventory/automobiles/new> | Automobile Creation Form |
| <http://localhost:3000/sales-department/salespeople> | Salespeople List |
| <http://localhost:3000/sales-department/salespeople/new> | Salesperson Hiring Form |
| <http://localhost:3000/sales-department/customers> | Customer List |
| <http://localhost:3000/sales-department/customers/new> | New Customer Form |
| <http://localhost:3000/sales-department/sales> | Sales List |
| <http://localhost:3000/sales-department/sales/new> | New Sale Form |
| <http://localhost:3000/sales-department/history> | Sales Records |
| <http://localhost:3000/service-department/technicians> | Technician List/Firing Page |
| <http://localhost:3000/service-department/technicians/new> | Technician Hiring Form |
| <http://localhost:3000/service-department/appointments> | Service Appointment/ Status List |
| <http://localhost:3000/service-department/appointments/new> | Appointment Scheduling Form |
| <http://localhost:3000/service-department/history> | Service History |

## API Endpoints

-------

## Inventory Microservice

**Manufacturers:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8100/api/manufacturers/>| GET | List of Manufacturers | ```{"manufacturers": [{"href": "", "id": 6, "name": "","#_on_lot": 4,"#_sold": 1}, {...}]}``` |
|<http://localhost:8100/api/manufacturers/:id/>| GET | Specific Manufacturer by ID | ```{"href": "", "id": 6, "name": "", "#_on_lot": 4, "#_sold": 1}``` |
|<http://localhost:8100/api/manufacturers/>| POST | Create a New Manufacturer | ```{"name": "Mercury"}``` |
|<http://localhost:8100/api/manufacturers/:id/>| PUT | Update Manufacturer by ID | ```{"name": ""}``` |
|<http://localhost:8100/api/manufacturers/:id/>| DELETE | Delete Manufacturer by ID | N/A |

**Models:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8100/api/models/>| GET | List of Models | ```{"models": [{"href": "", "id": 5, "name": "", "picture_url": "", "manufacturer": {"href": "", "id": 7, "name": "", "#_on_lot": 1, "#_sold": 0}},{...}]}``` |
|<http://localhost:8100/api/models/:id/>| GET | Specific Model by ID | ```{"href": "", "id": 5, "name": "", "picture_url": "", "manufacturer": {"href": "", "id": 7, "name": "", "#_on_lot": 1, "#_sold": 0}}``` |
|<http://localhost:8100/api/models/>| POST | Create a New Model | ```{"name": "Cougar", "picture_url": "http//picUrl.jpg", "manufacturer_id": 6}``` |
|<http://localhost:8100/api/models/:id/>| PUT | Update Model by ID | ```{"name": "", "picture_url": "", "manufacturer_id": 6}``` |
|<http://localhost:8100/api/models/:id/>| DELETE | Delete Model by ID | N/A |

**Automobiles:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8100/api/automobiles/>| GET | List of Automobiles | ```{"autos": [{"href": "", "id": 7, "color": "", "year": 1969, "vin": "", "sold": false, "model": {"href": "", "id": 5, "name": "", "picture_url": "", "manufacturer": {"href": "", "id": 7, "name": "", "#_on_lot": 1, "#_sold": 0}}}, {...}]}``` |
|<http://localhost:8100/api/automobiles/:vin/>| GET | Specific Automobile by VIN | ```{"href": "", "id": 7, "color": "", "year": 1969, "vin": "", "sold": false, "model": {"href": "", "id": 5, "name": "", "picture_url": "", "manufacturer": {"href": "", "id": 7, "name": "", "#_on_lot": 1, "#_sold": 0}}}``` |
|<http://localhost:8100/api/automobiles/>| POST | Create a New Automobile | ```{"color": "Blue", "year": 1967, "vin": "1C3CC5FB2AN120174", "model_id": 4}``` |
|<http://localhost:8100/api/automobiles/:vin/>| PUT | Update Automobile by VIN | ```{"color": "", "year": 2012, "sold": true}``` |
|<http://localhost:8100/api/automobiles/:vin/>| DELETE | Delete Automobile by VIN | N/A |

## Sales Microservice

**Sales:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8090/api/sales/>| GET | List of Sales | ```{"sales": [{"href": "", "id": 1, "automobile": {"href": "","import_href": "", "vin": "", "sold": false}, "salesperson": {"href": "", "id": 1, "first_name": "", "last_name": "","employee_id": 42}, "customer": {"href": "", "id": 1, "first_name": "", "last_name": "", "address": "", "phone_number": ""},"price": ""}, {...}]}``` |
|<http://localhost:8090/api/sales/>| POST | Create a Sale | ```{"automobile": "JT4RN61D8F5061251", "salesperson": "42", "customer": "1", "price": "55555"}``` |
|<http://localhost:8090/api/sales/:id/>| DELETE | Delete Sale by ID | N/A |

**Customers:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8090/api/customers/>| GET | List of Customers | ```{"customers": [{"href": "", "id": 1, "first_name": "", "last_name": "", "address": "", "phone_number": ""}, {...}]}``` |
|<http://localhost:8090/api/customers/>| POST | Create a Customer | ```{"first_name": "Doc", "last_name": "Brown", "address": "fake address", "phone_number": "555-555-5555"}``` |
|<http://localhost:8090/api/customers/:id/>| DELETE | Delete Customer by ID | N/A |

**Salespeople:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8090/api/salespeople/>| GET | List of Salespeople | ```{"salespeople": [{"href": "","id": 1,"first_name": "","last_name": "","employee_id": 42}, {...}]}``` |
|<http://localhost:8090/api/salespeople/>| POST | Create a Salesperson | ```{"first_name": "Doc", "last_name": "Brown", "employee_id": 42}``` |
|<http://localhost:8090/api/salespeople/:employee_id/>| DELETE | Delete Salesperson by employee_id | N/A |

## Services

**Technicians:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8080/api/technicians/>| GET | List of Technicians | ```{"Technicians": [{"first_name": "","last_name": "","employee_id": 1}, {...}]}``` |
|<http://localhost:8080/api/technicians/>| POST | Hire a Technician | ```{"first_name": "David", "last_name": "Freiburger"}``` |
|<http://localhost:8080/api/technicians/employee_id/>| DELETE | Fire Technician by employee_id | N/A |

**Service Appointments:**

| URL | Method | Action | JSON Body/Response |
| --- | :----: | -------- | :--- |
|<http://localhost:8080/api/appointments/>| GET | List of Service Appointments | ```{"Appointments": [{"id": 42, "reason": "", "vin": "", "customer_href": "", "status": "Pending", "technician": "", "date": "", "time": "", "vip": "No"}, {...}]}``` |
|<http://localhost:8080/api/appointments/>| POST | Hire a Technician | ```{"date_time": "2000-10-31T01:30:00.000-05:00", "reason": "C/S Shits straight borked", "vin": "1C3CC5FB2AN120174", "customer_href": 1,  "technician": 1}``` |
|<http://localhost:8080/api/appointments/:id/cancel/>| PUT | Set Appointment Status to "Canceled" by ID | N/A |
|<http://localhost:8080/api/appointments/3/finish/>| PUT | Set Appointment Status to "Finished" by ID | N/A |
|<http://localhost:8080/api/appointments/:id/>| DELETE | Delete Appointment by id | N/A |

## Design

-----

## Application Architecture and Design

![Diagram Description](diagram.png)

## Service Design

The "Service Microservice" provides CRUD functionality for the various features of a car dealership's service-department administration application, including Technician hiring and firing, service appointment scheduling, appointment status updates, and comprehensive service history. In order to implement certain features, congruency with the automobile inventory is required, and facilitated using a Polling function to populate a database with Automobile Value Objects. These VO's contain the VIN and Sold status of each car in the inventory database and are used to assign VIP status to customers who's cars exist inside the dealership's inventory.

## Sales Design

## Sales microservice
The sales microservice contains 4 models, A Salesperson ,Cutomer,AutomobileVO, and Sales model
1. SalesPerson: properties include first name, last name, and employee id.

2. Customer: properties include first name, last name, phone number, and address

3. Sale: properties include price; The Sale model shares a one to many relationship with AutomobileVO, Salesperson, and Customer, using the foreignkey. This allows you to pull data and create a sale  with one Car, salesperson and customer.

4. AutomobileVO: properties includes a vin and sold field. This is a Value Object, its purpose is to poll data from the Inventory microservices database. It checks and updates the data, so Sales Microservice can track whats availabe.
